#!/bin/bash

usage() {
  echo "Usage: $0 <alphanet|babylonnet|carthagenet|mainnet|zeronet>"
}

case "$1" in
  alphanet)
    ;;
  babylonnet)
    ;;
  carthagenet)
    ;;
  mainnet)
    ;;
  zeronet)
    ;;
  *) usage
    exit 1
    ;;
esac

NET=$1
echo "Preparing to build $NET"

# env: Ubuntu >= 16.04
apt update || exit 1
apt install -y rsync git m4 build-essential patch unzip wget libev-dev libgmp-dev pkg-config libhidapi-dev || exit 1
wget http://security.ubuntu.com/ubuntu/pool/main/b/bubblewrap/bubblewrap_0.2.1-1ubuntu0.1_amd64.deb || exit 1
dpkg -i bubblewrap_0.2.1-1ubuntu0.1_amd64.deb || exit 1
wget https://github.com/ocaml/opam/releases/download/2.0.1/opam-2.0.1-x86_64-linux || exit 1

cp opam-2.0.1-x86_64-linux /usr/local/bin/opam || exit 1
chmod a+x /usr/local/bin/opam

git clone https://gitlab.com/tezos/tezos.git || exit 1
cd tezos
git checkout $NET || exit 1
cat >./tezos-version <<EOF
#!/bin/bash
echo `git log --pretty=format:"%h" -1`
EOF
chmod +x tezos-version
./tezos-version
chmod a+rwx ~/tezos
opam init --bare || exit 1
make build-deps || exit 1
eval $(opam env) || exit 1
make || exit 1

echo "Done"
