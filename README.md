# Tezos Node Installer

Greetings and welcome to our humble repository for a simple in-house series of code which want to share with the community (hopefully you can help to improve it too!). Tezos Node 
Installer is a collection of scripts compiled into one which allows you to very quickly have a Tezos Node up and running on your system. Our record time for getting something working 
is less than 50 seconds.

How it works

- Our scripts automatically check the main Tezos repository for updated source code every Sunday and send them through the compilation process before saving the final binaries
- The current build environment is only for Ubuntu >= 16.04 and hopefully we can support more distros soon (hint, you can contribute too)

## Installation Instructions

Option 1 - Automated Installation
- (Optional but important) Ensure that you are downloading a safe version of our script install.sh. It has been MD5 Signed 37fd66ffbaf97da78ae1ef23b9d0fd2a
- Run the command:

```
  curl "https://gitlab.com/tezos-southeast-asia/tezosinstaller/raw/master/install.sh" | bash -s <alphanet|babylonnet|mainnet|zeronet|carthagenet> [prefix]
```

For example, if you want to download binaries for the mainnet, run the following command

```
curl "https://gitlab.com/tezos-southeast-asia/tezosinstaller/raw/master/install.sh" | bash -s mainnet
```
To verify the version you have just downloaded, run "tezos-version" in SSH and it should echo back the current build version
```
tezos-version  # see latest git hash for version
```

Option 2 - Manual Binary Download

You can download the binary files manually using the following command

```
https://gitlab.com/tezos-southeast-asia/tezosinstaller/-/jobs/artifacts/master/download?job=mainnet-build
```

Bear in mind that if you downloaded just the binaries manually, your OS might be missing some important pre-requisite modules for the node to run optimall. In this case, 
you may want to install the following packages on the OS.

```
apt install -y libhidapi-hidraw0 libhidapi-libusb0 libev-dev libgmp-dev zip
```

## Build From the Script Manually
```
./scripts/tzbuild.sh <alphanet|babylonnet|mainnet|zeronet|carthagenet>
```
## Firing up the node

If you have made it here thus far, good for you! That means you have now successfully completed downloading the binaries into your OS and are ready to start the tezos node. What 
the installation script has done is also to set global environment variables such that when you issue the "tezos-node" command, it calls it up from /usr/local/bin/tezos-node 

For a fresh installation, remember to run "tezos-node identity generate" followed by your choice of command line parameters. In order to run the node in the background with 
the RPC API listening on localhost port 8732, you can issue the following command

```
"nohup tezos-node run --rpc-addr 127.0.0.1:8732 --connections 10 &"
```

Feel free to play around with this script and report any issues you see to us! 
