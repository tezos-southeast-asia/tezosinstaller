#!/bin/bash

usage() {
  echo "Usage: $0 <alphanet|babylonnet|carthagenet|mainnet|zeronet> [prefix]"
}

case "$1" in
  alphanet)
    ;;
  babylonnet)
    ;;
  carthagenet)
    ;;
  mainnet)
    ;;
  zeronet)
    ;;
  *) usage
    exit 1
    ;;
esac

NET=$1
TEMP_DIR=$(mktemp -d)
PREFIX=${2-"/usr/local/bin"}

echo "preparing $NET tezos-*  app"

echo ================================
echo downloading pre-compiled tezos-* app
echo ================================

curl -L "https://gitlab.com/api/v4/projects/11806727/jobs/artifacts/master/download?job=${NET}-build" --output ${TEMP_DIR}/tezos.zip

echo ================================
echo installing lib for tezos-* app
echo ================================
sudo apt update
sudo apt install -y libhidapi-hidraw0 libhidapi-libusb0 libev-dev libgmp-dev zip

echo ================================
echo untar zip
echo ================================

unzip ${TEMP_DIR}/tezos.zip -d ${TEMP_DIR} || exit 1

echo ================================
echo installing tezos-* app
echo ================================
sudo cp ${TEMP_DIR}/tezos/* ${PREFIX}/ || exit 1

echo ================================
echo clearup temp file
echo ================================
rm -fr ${TEMP_DIR} || exit 1

echo ================================
echo tezos-version to check the latest git hash
echo ================================
${PREFIX}/tezos-version

echo ================================
echo finish
echo ================================
